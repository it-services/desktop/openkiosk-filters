# OpenKiosk Filters

This project includes configuration files for kiosk stations using OpenKiosk.

# Usage

Create a new txt file with the URL filters you need.  Set OpenKiosk to use it by going to the administrative screen, select OpenKiosk, then click the Filters tab.